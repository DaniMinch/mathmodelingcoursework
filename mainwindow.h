#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <calculations.h>
#include <aboutapp.h>
#include <optimalgowindow.h>

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Calculations<double, QDoubleSpinBox>* calcDouble;
    Calculations<int, QSpinBox>* calcInt;

private slots:
    void openAbout(){AboutApp *ap = new AboutApp;ap->show();}
    void openOptAlgo(){OptimAlgoWindow *wdw = new OptimAlgoWindow; wdw->show();
                      this->setWindowState(Qt::WindowMinimized);}
    void openHistogram();
    void optimizeUserFunction();
    void showTableY();
    void applyNoise();
    void setVariables();
    void setAutoRefresh(bool);
    void drawGraphics();
    void resetInitial();

};

#endif // MAINWINDOW_H
