#include "aboutapp.h"
#include "ui_aboutapp.h"

AboutApp::AboutApp(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutApp)
{
    ui->setupUi(this);
    setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setAttribute(Qt::WA_DeleteOnClose);
}

AboutApp::~AboutApp()
{
    delete ui;
}

void AboutApp::on_buttonBox_accepted()
{
    this->destroy();
}
