#ifndef HISTOGRAMWINDOW_H
#define HISTOGRAMWINDOW_H

#include <QMainWindow>
#include <chrono>
#include <QMessageBox>
#include <QTime>
#include <QCoreApplication>

static QVector<double> generateRandomVector(double deltaY = 0.5,
                                            int distributionType = 1,
                                            //0-Uniform, 1- Normal, 2- Triangular
                                            int rolls = 10000,
                                            // количество генераций
                                            bool isStatistical = false,
                                            // T- возвращает значения,F - их статистику
                                            double intervals = 10
        // количество интервалов для статистической
        )
{
    QVector<double> result;// = new QVector<double>;
    if(isStatistical)
        result.fill(0, intervals);
    QTime dieTime= QTime::currentTime().addMSecs(1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    std::mt19937 generator(std::chrono::system_clock::now().
                           time_since_epoch().count());
    switch(distributionType){
    case 0:
    {
        std::uniform_real_distribution<double> distribution
                (-deltaY,deltaY);
        for(int i =0; i<rolls; ++i)
        {
            double number = distribution(generator);
            if(isStatistical)
                ++(result[int((number/deltaY+1)*(intervals/2))]);
            else
                result.push_back(number);
        }
        break;
    }
    case 1:
    {
        std::normal_distribution<double> distribution(0,deltaY);
        int valuesCount = 0;
        do
        {
            double number = distribution(generator);
            if((number>=-deltaY)&&(number<deltaY))
            {++valuesCount;
                if(isStatistical)
                    ++result[int((number/deltaY+1)*(intervals/2))];
                else
                    result.push_back(number);
            }
            else if (number == deltaY)
            {
                ++valuesCount;
                if(isStatistical)
                    ++result[intervals-1];
                else
                    result.push_back(number);
            }
        } while (valuesCount < rolls);
        break;
    }
    case 2:
    {
        // сделано через получение значений нормального распределения
        // см. английскую вики Generating Triangular-distributed random variates
        // a= -deltaY, b = deltaY, c = 0 = 2/(b-a) =мода
        double Fc = 0.5; // 0-(-deltaY)/deltaY-(-deltaY);
        std::uniform_real_distribution<double> distribution(0,1);
        for(int i = Fc; i<rolls; ++i)
        {
            double number = distribution(generator);
            if(number < Fc)
                number = -deltaY + sqrt(number * 2 * deltaY* deltaY);
            else
                number = deltaY - sqrt((1 - number) * 2 * deltaY* deltaY);
            if (isStatistical)
                ++result[int((number/deltaY+1)*(intervals/2))];
            else
                result.push_back(number);
        }
        break;
    }
    default:
        QMessageBox::critical(NULL,"Ошибка выбора генератора",
                              QString("Невозможно выбрать генератор. Обратитесь"
                                      " к разработчику за консультацией."));
    }
    return result;
}

namespace Ui {
class HistogramWindow;
}

class HistogramWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit HistogramWindow(double maxElem = 1.0,
                             int distr = 1, int coef = 0,
                             QWidget *parent = 0);
    ~HistogramWindow();

private slots:
    void drawHistogram();

private:
    Ui::HistogramWindow *ui;
};

#endif // HISTOGRAMWINDOW_H
