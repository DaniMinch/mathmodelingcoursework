#include "histogramwindow.h"
#include "ui_histogramwindow.h"

HistogramWindow::HistogramWindow(double maxElem, int distr, int coef,
                                 QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HistogramWindow)
{
    ui->setupUi(this);
    ui->dblSpinMaxYT->setValue(maxElem);
    ui->cmbDistribution->setCurrentIndex(distr);
    ui->cmbCoef->setCurrentIndex(coef);
    ui->tableValues->horizontalHeader()->setSectionResizeMode
            (QHeaderView::ResizeToContents);
    connect(ui->btnDraw,SIGNAL(clicked(bool)),this,SLOT(drawHistogram()));
    drawHistogram();
}

HistogramWindow::~HistogramWindow()
{
    delete ui;
}

/*SLOT*/void HistogramWindow::drawHistogram()
{
    double intervals = ui->spinIntervals->value();
    QLocale lcl(QLocale::Russian);
    double deltaY = std::abs(ui->dblSpinMaxYT->value()*
                             lcl.toDouble(ui->cmbCoef->currentText()));
    QVector<double> bars = generateRandomVector(deltaY,
                                                ui->cmbDistribution->currentIndex(),
                                                ui->spinRolls->value(),
                                                TRUE,
                                                intervals);
    ui->plotHistogram->clearPlottables();
    QCPBars * bar = new QCPBars(ui->plotHistogram->xAxis,
                                ui->plotHistogram->yAxis);
    ui->plotHistogram->addPlottable(bar);
    QVector<double> ticks;
    QVector<QString> labels;
    ui->tableValues->setRowCount(intervals);
    for(int i = 0; i < intervals; ++i)
    {
        ticks.push_back(i+1);
        QString label = QString(QString::number(-deltaY+(i*deltaY/(intervals/2)))+" / "+
                                QString::number(-deltaY+((i+1)*deltaY/(intervals/2))));
        labels.push_back(label);
        ui->tableValues->setItem(i,0,new QTableWidgetItem(label));
        ui->tableValues->setItem(i,1,new QTableWidgetItem(QString::number
                                                          (bars[i])));
    }
    ui->plotHistogram->xAxis->setAutoTicks(false);
    ui->plotHistogram->xAxis->setAutoTickLabels(false);
    ui->plotHistogram->xAxis->setTickVector(ticks);
    ui->plotHistogram->xAxis->setTickVectorLabels(labels);
    ui->plotHistogram->xAxis->setTickLabelRotation(60);
    ui->plotHistogram->xAxis->setSubTickCount(0);
    ui->plotHistogram->xAxis->setTickLength(0, 4);
    ui->plotHistogram->xAxis->setRange(0, intervals+1);
    if(((bars[int(intervals/2) - 1] +
         bars[int(intervals / 2)]) * 0.55) > ((bars[int(intervals / 2)])*1.1))
        ui->plotHistogram->yAxis->setRange(0, (bars[int(intervals/2) - 1] +
                                           bars[int(intervals / 2)]) * 0.55);
    else
        ui->plotHistogram->yAxis->setRange(0, bars[int(intervals / 2)] * 1.1);
    bar->setData(ticks, bars);
    ui->plotHistogram->replot();

}
