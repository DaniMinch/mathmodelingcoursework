#include "optimalgowindow.h"
#include "calculations.h"
#include "ui_optimalgowindow.h"

OptimAlgoWindow::OptimAlgoWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OptimAlgoWindow)
{
    ui->setupUi(this);
    connect(ui->radioRos,SIGNAL(toggled(bool)),
            this,SLOT(chooseFormula(bool)));
    connect(ui->actionAbout,SIGNAL(triggered(bool)),
            this,SLOT(showAbout()));
    connect(ui->btnApply,SIGNAL(clicked(bool)),
            this,SLOT(startOptimization()));
}

OptimAlgoWindow::~OptimAlgoWindow()
{
    delete ui;
}

// изменяет отображение компонентов в зависимости от выбранной формулы
/*SLOT*/void OptimAlgoWindow::chooseFormula(bool isRos)
{
    if(isRos)
    {
        ui->frameFormulaEllipse->setEnabled(false);
        ui->frameFormulaRos->setEnabled(true);
    }
    else
    {
        ui->frameFormulaEllipse->setEnabled(true);
        ui->frameFormulaRos->setEnabled(false);
    }
}

// функция выполняет оптимизацию целевой функций и рисует график
/*SLOT*/void OptimAlgoWindow::startOptimization()
{
    QVector<QVector<double>*> optimPoints;
    ui->btnApply->setEnabled(false);
    // рисуем изолинии
    drawIsolines(ui->radioRos->isChecked());
    // находим точки оптимизации

    if(ui->radioRos->isChecked())
        optimPoints = randomSearchOptimization(
                    0,ui->spinARos->value(),
                    ui->spinBRos->value(), ui->dblStartX->value(),
                    ui->dblStartY->value(), ui->dblSpinH->value(),
                    ui->dblSpinE->value());
    else
        optimPoints = randomSearchOptimization(
                    1, ui->spinAEllipse->value(),
                    ui->spinBEllipse->value(), ui->dblStartX->value(),
                    ui->dblStartY->value(), ui->dblSpinH->value(),
                    ui->dblSpinE->value());
    // рисуем ломаную оптимизации
    QCPCurve *crv= new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
    crv->setPen(QPen(QColor(255, 0, 0)));
    crv->setData(*optimPoints[0],*optimPoints[1]);
    ui->graph->addPlottable(crv);
    ui->graph->replot();
    ui->btnApply->setEnabled(true);
}

template<typename T, class Type>
class Calculations;

// функция находит точки по которым проходит оптимизация целевой функции методом
// случайного поиска
QVector<QVector<double>*> OptimAlgoWindow::randomSearchOptimization(
        int functionType, // 0 - Rosenbrock, 1 - Ellipsoid, 2 - Euler
        int A, int B,
        double startX, double startY,
        double H, double E)
{
    // задание вектора значений
    QVector<QVector<double>*> result(2);
    result[0] = new QVector<double>;
    result[0]->clear();
    result[1] = new QVector<double>;
    result[1]->clear();
    // генератор с равномерным распределением
    std::mt19937 gen(time(NULL));
    std::uniform_int_distribution<int> distrDirection(0,7);
    // fn1 - пробное значение, fn - текущее
    double fn1 = double(), fn = double(), X = startX, Y = startY;
    // в переменной хранится номер элемента в списке направлений
    // а не само направление
    int choosenDirection = int();
    bool isPossibleNext = true;
    if(functionType == 0)
        fn = pow(A - X, 2)+ (B * pow(Y - (X * X),2));
    else
        fn = pow(X / A, 2) + pow(Y / B, 2);
    fn1 = fn;
    QList<int> directions;
    int iterationsCount = 0;
    do
    {
        ++iterationsCount;
        if(!isPossibleNext)
        {
            // если пробное значение не подошло восстававливаем текущее и
            // выкидываем неверное напраление
            X = result.at(0)->last();
            Y = result.at(1)->last();
            directions.erase(directions.begin()+choosenDirection);
        }
        else
        {
            // если пробная переменная подошла, записываем ее в вектор
            // результатов, обновляем значение функции и список направлений
            directions.clear();
            for (int i = 0; i < 8; ++i)
                directions.push_back(i);
            result.at(0)->push_back(X);
            result.at(1)->push_back(Y);
            fn = fn1;
        }
        // генерируем случайное направление
        choosenDirection = distrDirection(gen)%directions.size();
        switch (directions[choosenDirection])
        {
        case 0:
            X -= H;
            break;
        case 1:
            X -= H;
            Y += H;
            break;
        case 2:
            Y += H;
            break;
        case 3:
            X += H;
            Y += H;
            break;
        case 4:
            X += H;
            break;
        case 5:
            X += H;
            Y -= H;
            break;
        case 6:
            Y -= H;
            break;
        case 7:
            X -= H;
            Y -= H;
            break;
        default:
            QMessageBox::critical(this,"Ошибка генератора", "Что-то пошло не "
                                                            "так при генерации "
                                                            "направления. Возмо"
                                                            "жно, ошибка "
                                                            "генератора");
            break;
        }
        if(functionType == 0)
            fn1 = pow(A - X, 2)+ (B * pow(Y - (X * X),2));
        else
            fn1 = pow(X / A, 2) + pow(Y / B, 2);
        // проверяем значение функци с учетом погрешности
        // а также наличие этих точек в векторе
        if ((fn1+pow(E,2) < fn)&&(!(result.at(0)->contains(X)&&
                                    result.at(1)->contains(Y))))
            isPossibleNext = true;
        else
            isPossibleNext = false;
    }
    // выход если модуль изменения нового значения меньше точности
    // или нет возможных путей
    while (!(((std::abs(fn1 - fn) < E)&&(isPossibleNext)) ||
             ((!isPossibleNext)&&(directions.size()==1))));
    if (std::abs(fn) < (E/100))
        fn = 0;
    int precision = 1;
    while (E < 1)
    {
        ++precision;
        E *= 10;
    }
    double lastX = result.at(0)->last(),lastY = result.at(1)->last();
    if (std::abs(lastX)<(E/100))
        lastX = 0.0;
    if (std::abs(lastY)<(E/100))
        lastY = 0.0;
    ui->editIterationsCount->setText(QString::number(iterationsCount));
    ui->editXFinish->setText(QString::number(lastX,'g', precision));
    ui->editYFinish->setText(QString::number(lastY,'g', precision));
    ui->editZFinish->setText(QString::number(fn));
    return result;
}

// переопредление для пользовательской функции
QVector<QVector<double>*> OptimAlgoWindow::randomSearchOptimization(
        EulerData* data)
{
    // задание вектора значений
    QVector<QVector<double>*> result(3);
    result[0] = new QVector<double>;
    result[0]->clear();
    result[1] = new QVector<double>;
    result[1]->clear();
    result[2] = new QVector<double>;
    result[2]->clear();
    // генератор с равномерным распределением
    std::mt19937 gen(time(NULL));
    std::uniform_int_distribution<int> distrDirection(0,7);
    // fn1 - пробное значение, fn - текущее
    double fn1 = double(), fn = double(),
            X = data->value->getVariableValue(ValTable<double>::FIRST),
            Y = data->value->getVariableValue(ValTable<double>::SECOND);
    double H = 0.01, E = 0.00001;
    int parseXPosition = 0, parseYPosition = 0;
    // в переменной хранится номер элемента в списке направлений
    // а не само направление
    int choosenDirection = int();
    bool isPossibleNext = true;
    Calculations<double,QDoubleSpinBox>* calc =
            new Calculations<double,QDoubleSpinBox>;
    std::vector<double>* ym = calc->calcEuler
            (calc->parseString(data->nominator, parseXPosition, data->value),
             calc->parseString(data->denominator, parseYPosition, data->value),
             data->h, data->xt, data->n);
    for(int i = 0; i<data->yN.size(); ++i)
        fn += std::abs(data->yN[i] - (*ym)[i]);
        //fn += std::pow(data->yN[i] - (*ym)[i],2);
    fn1 = fn;
    QList<int> directions;
    int iterationsCount = 0;
    do
    {
        ++iterationsCount;
        if(!isPossibleNext)
        {
            // если пробное значение не подошло восстававливаем текущее и
            // выкидываем неверное напраление
            X = result.at(0)->last();
            Y = result.at(1)->last();
            directions.erase(directions.begin()+choosenDirection);
        }
        else
        {
            // если пробная переменная подошла, записываем ее в вектор
            // результатов, обновляем значение функции и список направлений
            directions.clear();
            for (int i = 0; i < 8; ++i)
                directions.push_back(i);
            result.at(0)->push_back(X);
            result.at(1)->push_back(Y);
            result.at(2)->push_back(fn1/data->yN.size());
            fn = fn1;
        }
        // генерируем случайное направление
        choosenDirection = distrDirection(gen)%directions.size();
        switch (directions[choosenDirection])
        {
        case 0:
            X -= H;
            break;
        case 1:
            X -= H;
            Y += H;
            break;
        case 2:
            Y += H;
            break;
        case 3:
            X += H;
            Y += H;
            break;
        case 4:
            X += H;
            break;
        case 5:
            X += H;
            Y -= H;
            break;
        case 6:
            Y -= H;
            break;
        case 7:
            X -= H;
            Y -= H;
            break;
        default:
            QMessageBox::critical(this,"Ошибка генератора", "Что-то пошло не "
                                                            "так при генерации "
                                                            "направления. Возмо"
                                                            "жно, ошибка "
                                                            "генератора");
            break;
        }
        parseXPosition = 0, parseYPosition = 0;
        data->value->setVariableValue(ValTable<double>::FIRST, X);
        data->value->setVariableValue(ValTable<double>::SECOND, Y);
        std::vector<double>* yMn = calc->calcEuler
                (calc->parseString(data->nominator, parseXPosition, data->value),
                 calc->parseString(data->denominator, parseYPosition, data->value),
                 data->h, data->xt, data->n);
        fn1 = 0;
        for(int i = 0; i<data->yN.size(); ++i)
            fn1  += std::abs(data->yN[i] - (*yMn)[i]);
            //fn1 += std::pow(data->yN[i] - (*yMn)[i],2);
        // проверяем значение функци с учетом погрешности
        // а также наличие этих точек в векторе
        if ((fn1+pow(E,2) < fn)&&(!(result.at(0)->contains(X)&&
                                    result.at(1)->contains(Y))))
            isPossibleNext = true;
        else
            isPossibleNext = false;
    }
    // выход если модуль изменения нового значения меньше точности
    // или нет возможных путей
    while (!(((std::abs(fn1 - fn) < E)&&(isPossibleNext)) ||
             ((!isPossibleNext)&&(directions.size()==1))));

    qDebug()<<(std::abs(fn1 - fn) < E)<<isPossibleNext<<directions.size()<<choosenDirection;
    if (std::abs(fn) < (E/100))
        fn = 0;
    int precision = 1;
    while (E < 1)
    {
        ++precision;
        E *= 10;
    }
    double lastX = result.at(0)->last(),lastY = result.at(1)->last();
    if (std::abs(lastX)<(E/100))
        lastX = 0.0;
    if (std::abs(lastY)<(E/100))
        lastY = 0.0;
    // обновление массива данных
    data->value->setVariableValue(ValTable<double>::FIRST, lastX);
    data->value->setVariableValue(ValTable<double>::SECOND, lastY);
    // вывод значений в компоненты
    ui->editIterationsCount->setText(QString::number(iterationsCount));
    ui->editXFinish->setText(QString::number(lastX,'g', precision));
    ui->editYFinish->setText(QString::number(lastY,'g', precision));
    ui->editZFinish->setText(QString::number(fn/data->yN.size()));
    return result;
}

// функция отрисовывает изолинии одной из двух целевых функций
void OptimAlgoWindow::drawIsolines(bool isRosenbrock = false)
{
    // коэфициенты методов
    long double A = 0, B = 0;
    if (isRosenbrock)
    {
        A = double(ui->spinARos->value());
        B = double(ui->spinBRos->value());
    }
    else
    {
        A = double(ui->spinAEllipse->value());
        B = double(ui->spinBEllipse->value());
    }
    if(((std::abs(A)>0.00001)&&(std::abs(B)>0.00001))||(isRosenbrock))
    {
        // установка границ графиков и шага для определения точек изолиний
        long double xmax = ui->dblXMaxAxis->value();
        long double xmin = ui->dblXMinAxis->value();
        long double hXIsolines = (xmax-xmin)/10000.0;
        long double ymax = ui->dblYMaxAxis->value();
        long double ymin = ui->dblYMinAxis->value();
        // количество изолиний и интервал между ними
        int isoCount = ui->spinIsoCount->value()+1;
        long double isoInt = ui->dblSpinIsoInt->value();
        // вектора содержащие значения точек изолиний для
        // верхней и нижней части каждой изолинии
        QVector<QVector<double>*> vctX(isoCount);
        QVector<QVector<double>*> vctYpos(isoCount);
        QVector<QVector<double>*> vctYneg(isoCount);
        for(int i = 0; i<isoCount; ++i)
        {
            vctX[i] = new QVector<double>;
            vctYpos[i] = new QVector<double>;
            vctYneg[i] = new QVector<double>;
        }
        // очистка графика и установка новых границ
        ui->graph->clearPlottables();
        ui->graph->xAxis->setRange(xmin,xmax);
        ui->graph->yAxis->setRange(ymin,ymax);
        long double ypos = ymin, yneg = ymin;
        // вектор для прорыва разрывов воззле оси y
        QVector<bool> eol(isoCount);
        eol.fill(true);
        // для всех X
        for(long double x = xmin; x <= xmax; x += hXIsolines)
            // для всех изолиний
            for(int i = 0; i < isoCount; ++i)
            {
                // изолинии рисуются из двух участков: верхней части и нижней
                // части, граница проходит по самой крайней левой/правой точке
                if (isRosenbrock)
                {
                    ypos = (-0.1) * std::sqrt((- x * x) + (2 * x) +
                                              (double(i) * isoInt) - 1)+ (x * x);
                    yneg = (0.1) * std::sqrt((- x * x) + (2 * x) +
                                             (double(i) * isoInt) - 1)+ (x * x);
                }
                else
                {
                    ypos = (B / A) * std::sqrt((A * A * (double(i) * isoInt)) -
                                               (x * x));
                    yneg = -(B / A) * std::sqrt((A * A * (double(i) * isoInt)) -
                                                (x * x));
                    // если точка является первой/последней на данном участке
                    // изолинии, то она соединяется с осью х
                    if((eol[i] && (!std::isnan(ypos)))||
                            ((!eol[i]) && (std::isnan(ypos))))
                    {
                        vctX.at(i)->push_back(x);
                        vctYpos.at(i)->push_back(0);
                        vctYneg.at(i)->push_back(0);
                    }
                    // проверка на наличие разрыва изолинии
                    if(std::isnan(ypos))
                        eol[i] = true;
                    else
                        eol[i] = false;
                }
                vctX.at(i)->push_back(x);
                vctYpos.at(i)->push_back(ypos);
                vctYneg.at(i)->push_back(yneg);
            }
        QCoreApplication::processEvents();
        // отрисовка участка и обнуление текущих значений
        for(int i = 0; i<isoCount; ++i)
        {
            QCPCurve *crvPos= new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
            ui->graph->addPlottable(crvPos);
            crvPos->setData(*vctX[i],*vctYpos[i]);
            QCPCurve *crvNeg= new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
            ui->graph->addPlottable(crvNeg);
            crvNeg->setData(*vctX[i],*vctYneg[i]);
        }
        for(int i = 0; i<isoCount; ++i)
        {
            vctX[i] = new QVector<double>;
            vctYpos[i] = new QVector<double>;
            vctYneg[i] = new QVector<double>;
        }
        ui->graph->replot();
    }
    else
        QMessageBox::critical(this,"Ошибка","Коэфициенты не могут быть равны 0");
    return;
}

// функция для оптимизации пользовательской функции
ValTable<double>* OptimAlgoWindow::optimizeUserFunction(EulerData *data)
{
    // установка значений компонентов
    ui->dblSpinE->setValue(0.00001);
    ui->dblSpinH->setValue(0.01);
    ui->spinIsoCount->setValue(20);
    ui->dblStartX->setValue(data->value->getVariableValue
                            (ValTable<double>::FIRST));
    ui->dblStartY->setValue
            (data->value->getVariableValue(ValTable<double>::SECOND));
    ui->radioUserFunction->setChecked(true);
    ui->groupData->setEnabled(false);
    // вызов алгоритма оптимизации
    QVector<QVector<double>*> optimPoints = randomSearchOptimization(data);
    // опредление границ графика
    auto result = std::minmax_element
            (optimPoints[0]->begin(), optimPoints[0]->end());
    double xmin = optimPoints[0]->last()-((*result.second-*result.first)*1.3);
    double xmax = optimPoints[0]->last()+((*result.second-*result.first)*1.3);
    ui->dblXMinAxis->setValue(xmin);
    ui->dblXMaxAxis->setValue(xmax);
    ui->graph->xAxis->setRange(xmin, xmax);
    result = std::minmax_element
            (optimPoints[1]->begin(), optimPoints[1]->end());
    double ymin = optimPoints[1]->last()-((*result.second-*result.first)*1.3);
    double ymax = optimPoints[1]->last()+((*result.second-*result.first)*1.3);
    ui->dblYMinAxis->setValue(ymin);
    ui->dblYMaxAxis->setValue(ymax);
    ui->graph->yAxis->setRange(ymin, ymax);
    // подсчет поля значений для изолиний
    double X,Y;
    QVector<Points> points;
    Calculations<double,QDoubleSpinBox>* calc =
            new Calculations<double,QDoubleSpinBox>;
    int pointcount = 200;
    for(int i = 0; i < pointcount; ++i)
    {
        X = xmin+((xmax - xmin) * i / (pointcount+1));
        for(int j = 0; j < pointcount; ++j)
        {
            Y = ymin+((ymax - ymin) * j / (pointcount+1));
            int parseXPosition = 0, parseYPosition = 0;
            data->value->setVariableValue(ValTable<double>::FIRST, X);
            data->value->setVariableValue(ValTable<double>::SECOND, Y);
            std::vector<double>* ym = calc->calcEuler
                    (calc->parseString(data->nominator, parseXPosition, data->value),
                     calc->parseString(data->denominator, parseYPosition, data->value),
                     data->h, data->xt, data->n);
            double Z = 0;
            for(int i = 0; i<data->yN.size(); ++i)
                Z  += std::abs(data->yN[i] - (*ym)[i]);
                //Z += std::pow(data->yN[i] - (*ym)[i],2);
            Points pts;
            pts.x = X;
            pts.y = Y;
            pts.z = Z/data->yN.size();
            points.push_back(pts);
        }
    }
    // возвращение значений результата в таблицу значений
    data->value->setVariableValue(ValTable<double>::FIRST,
                                  ui->editXFinish->text().toDouble());
    data->value->setVariableValue(ValTable<double>::SECOND,
                                  ui->editYFinish->text().toDouble());
    // построение изолиний
    std::sort(points.begin(),points.end(),lessPoint);
    double interval = std::abs(points.first().z-points.last().z)/20;
    ui->dblSpinIsoInt->setValue(interval);
    for (int j = 1; j < 21; ++j)
    {
        //получаем отсортированный вектор значений,
        // находящихся внутри каждой изолинии
        QVector<Points>* isoX = new QVector<Points>;
        isoX->clear();
        for (const auto& i: points)
        {
            if (i.z<points.first().z+interval*j)
                isoX->push_back(i);
        }
        std::sort(isoX->begin(), isoX->end(), lessX);
        // создаем вектора верхнего и нижнего участка изолиний
        QVector<double>* Yabove = new QVector<double>;
        Yabove->clear();
        Yabove->push_back((*isoX)[0].y);
        QVector<double>* Ybelow = new QVector<double>;
        Ybelow->clear();
        Ybelow->push_back((*isoX)[0].y);
        QVector<double>* Xabove = new QVector<double>;
        Xabove->clear();
        Xabove->push_back((*isoX)[0].x);
        QVector<double>* Xbelow = new QVector<double>;
        Xbelow->clear();
        Xbelow->push_back((*isoX)[0].x);
        for (int i = 1; i < isoX->size(); ++i)
        {
            // если промежуток между точкками больше 2-х интервалов, то разрыв
            // еще должна быть проврека на крайние значения, но она не работает
            if((std::abs(Xabove->last()-(*isoX)[i].x)>std::abs((xmax-xmin)/50))||
                    (((*isoX)[i].y==ymax)&&((*isoX)[i-1].y!=ymax)))
            {
                QCPCurve *crv = new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
                crv->setData(*Xabove,*Yabove);
                ui->graph->addPlottable(crv);
                Xabove->clear();
                Yabove->clear();
                Xabove->push_back((*isoX)[i].x);
                Yabove->push_back((*isoX)[i].y);
                crv = new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
                crv->setData(*Xbelow,*Ybelow);
                ui->graph->addPlottable(crv);
                Xbelow->clear();
                Ybelow->clear();
                Xbelow->push_back((*isoX)[i].x);
                Ybelow->push_back((*isoX)[i].y);
            } else if(((*isoX)[i].x>(*isoX)[i-1].x)&&
                      (!(((*isoX)[i].x ==(*isoX)[i-1].x)&&
                         (((*isoX)[i].y == ymax)||
                          (*isoX)[i].y == ymin))))
            {
                // данные забиваются только при изменении x
                Xbelow->push_back((*isoX)[i].x);
                Ybelow->push_back((*isoX)[i].y);
                Xabove->push_back((*isoX)[i-1].x);
                Yabove->push_back((*isoX)[i-1].y);
            }
        }
        // сводим оба графа к одной точке
        Xbelow->push_back((*isoX)[isoX->size()-1].x);
        Ybelow->push_back((*isoX)[isoX->size()-1].y);
        Xabove->push_back((*isoX)[isoX->size()-1].x);
        Yabove->push_back((*isoX)[isoX->size()-1].y);
        // заносим в таблицу
        QCPCurve *crv1 = new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
        crv1->setData(*Xabove,*Yabove);
        QCPCurve *crv2 = new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
        crv2->setData(*Xbelow,*Ybelow);
        ui->graph->addPlottable(crv1);
        ui->graph->addPlottable(crv2);
    }
    // добавляем график оптимизации и перерсиовываем виджет
    QCPCurve *crv = new QCPCurve(ui->graph->xAxis, ui->graph->yAxis);
    crv->setPen(QPen(QColor(255, 0, 0)));
    crv->setData(*optimPoints[0],*optimPoints[1]);
    ui->graph->addPlottable(crv);
    ui->graph->replot();
//    QTableWidget * table = new QTableWidget(optimPoints[0]->size(),3);
//    for(int i=0; i<optimPoints[0]->size(); ++i)
//    {
//        table->setItem(i,0,new QTableWidgetItem(QString::number(optimPoints.at(0)->at(i))));
//        table->setItem(i,1,new QTableWidgetItem(QString::number(optimPoints.at(1)->at(i))));
//        table->setItem(i,2,new QTableWidgetItem(QString::number(optimPoints.at(2)->at(i))));
//    }
//    table->show();
    return data->value;
}
