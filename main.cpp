#include "mainwindow.h"
#include "histogramwindow.h"
#include "optimalgowindow.h"
#include "aboutapp.h"
#include "calculations.h"
#include "qcustomplot.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
