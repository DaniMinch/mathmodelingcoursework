#-------------------------------------------------
#
# Project created by QtCreator 2015-09-23T12:40:11
#
#-------------------------------------------------

QT       += core gui

CONFIG	 += C++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

TARGET = MathModelingCoursework
TEMPLATE = app


SOURCES += main.cpp\
	mainwindow.cpp \
    qcustomplot.cpp \
    aboutapp.cpp \
    optimalgowindow.cpp \
    histogramwindow.cpp

HEADERS  += mainwindow.h \
    delegate.h \
    calculations.h \
    qcustomplot.h \
    aboutapp.h \
    optimalgowindow.h \
    histogramwindow.h

FORMS    += mainwindow.ui \
    aboutapp.ui \
    optimalgowindow.ui \
    histogramwindow.ui

RC_FILE = ico.rc

RESOURCES += \
    resource.qrc
