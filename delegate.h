#ifndef DELEGATE_H
#define DELEGATE_H

#include <QStyledItemDelegate>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QCheckBox>

class ADelegate: public QStyledItemDelegate
{
    Q_OBJECT
public:
    ADelegate(QObject *parent = 0):QStyledItemDelegate(parent){}
};

template <class T>
class Delegate: public ADelegate
{
public:
    enum Type
    {
        NoEdit = 0,
        SpinBox = 1,
        CheckBox = 2,
    };

    Delegate(Type _type = SpinBox, QObject *parent = 0):
        type(_type),
        ADelegate(parent)
    {}

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const Q_DECL_OVERRIDE
    {
        QWidget *editor;
        switch (type) {
        case SpinBox:
        {
            T *spin = new T(parent);
            spin->setFrame(false); //отсутствие границы
            spin->setMinimum(-100);
            spin->setMaximum(100);
            if(std::is_same<T,QDoubleSpinBox>::value)
                spin->setSingleStep(0.1);
            else
                spin->setSingleStep(1);
            editor = static_cast<T*> (spin);
            break;
        }
        case CheckBox:
        {
            QCheckBox *chkbox = new QCheckBox(parent);
            chkbox->setChecked(false);
            editor = static_cast<QCheckBox*>(chkbox);
            break;
        }
        default:
            editor = nullptr;
        }
        return editor;
    }


    void setEditorData(QWidget *editor, const QModelIndex &index) const Q_DECL_OVERRIDE
    {
        switch(type){
        case SpinBox:
        {
            int value = index.model()->data(index, Qt::EditRole).toInt();

            T *spBox = static_cast<T*>(editor);
            spBox->setValue(value);
            break;
        }
        case CheckBox:
        {
            bool value = index.model()->data(index, Qt::EditRole).toBool();
            QCheckBox * chkbox = static_cast<QCheckBox*>(editor);
            chkbox->setChecked(value);
            break;
        }
        }

    }

    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const Q_DECL_OVERRIDE
    {
        QVariant value;
        switch(type)
        {
        case SpinBox:{
            T *spBox = static_cast<T*>(editor);
            spBox->interpretText();
            value = spBox->value();
            break;
        }
        case CheckBox:{
            QCheckBox* chkbox = static_cast<QCheckBox*>(editor);
            value = chkbox->isChecked();
            break;
        }
        }

        model->setData(index, value, Qt::EditRole);
    }

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE
    {
        qDebug()<<option.rect;
               qDebug()<<type;
        editor->setGeometry(option.rect);
    }
private:
    Type type;
};

#endif // DELEGATE_H
