#ifndef CALCULATIONS_H
#define CALCULATIONS_H

#include <QMainWindow>
#include "ui_mainwindow.h"
#include "histogramwindow.h"
#include "optimalgowindow.h"
#include <functional>
#include <algorithm>
#include <type_traits>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QMessageBox>
#include <QDebug>
#include <QFile>

template <typename T>
struct ValNode
{
    ValNode(QChar _name, T _value = 0, bool _isUnknown = false)
        :name(_name),value(_value),isUnknown(_isUnknown){}
    QChar name;
    T value;
    bool isUnknown;
};

template <typename T>
struct isExistingValNode:std::binary_function<ValNode<T>, QChar, bool>
{
    bool operator ()(ValNode<T>* v2, QChar ch)const{return (ch==v2->name);}
};


template <typename T>
class ValTable {
public:
    ValTable(){}

    enum Test
    {
        FIRST,
        SECOND
    };

    bool add(const QChar& ch,T val = 0, bool isUnknown = false)
    {
        if(isUnknown)
        {
            if(unknowncount < 2)
                ++unknowncount;
            else
                return false;
        }
        vals.push_back(new ValNode<T>(ch,val, isUnknown));
        return true;
    }

    bool contains(const QChar& ch) const
    {
        return (vals.end()!=std::search_n(vals.begin(),vals.end(),
                                          1, ch ,isExistingValNode<T>()));
    }

    void remove(const QChar& ch)
    {
        auto it = std::search_n(vals.begin(),vals.end(),
                                1, ch ,isExistingValNode<T>());
        if((*it)->isUnknown)
            --unknowncount;
        vals.erase(it);
    }

    void show() const
    {
        for(const auto& node:vals)
            qDebug()<<node->name<<node->value<<node->isUnknown;
    }

    T getValue(const QChar& ch)
    {
        auto it = std::search_n(vals.begin(),vals.end(),
                                1, ch ,isExistingValNode<T>());
        return (*it)->value;
    }

    T getVariableValue(Test var)
    {
        bool first = true;
        for (const auto& node:vals)
            if(node->isUnknown)
            {
                if((var == FIRST)||(!first))
                    return node->value;
                else if(first)
                    first = false;
            }
        return NULL;
    }

    void setVariableValue(Test var, T value)
    {
        bool first = true;
        for (const auto& node:vals)
            if(node->isUnknown)
            {
                if((var == FIRST)||(!first))
                    node->value = value;
                else if(first)
                    first = false;
            }
    }

    bool checkUnknownCount()
    {
        return (unknowncount==2);
    }

    ValTable<double>* toDouble()
    {
        ValTable<double>*  result = new ValTable<double>;
        for (const auto& node:vals)
            result->add(node->name,double(node->value),node->isUnknown);
        return result;
    }

private:
    std::vector<ValNode<T>*> vals;
    int unknowncount = 0;
};

template <typename T, class Type>
class Calculations
{
    //Q_OBJECT
public:
    explicit Calculations(Ui::MainWindow *_ui = nullptr,
                          ValTable<double>* _vals = new ValTable<double>):
        ui(_ui),
        vals(_vals),
        yT(nullptr),
        hst(nullptr),
        opt(nullptr),
        tableY(nullptr)
    {}

    // функция заносит все значения переменнных кроме s в таблицу
    // при каждом запуске таблица рисуется заново
    void setVariables(const QString str)
    {
        ui->checkAutoRefresh->setChecked(false);
        ui->btnChangeInitial->setEnabled(false);
        ui->groupNumType->setEnabled(false);
        ui->frameFormula->setEnabled(false);
        while(ui->tableValues->rowCount())
            ui->tableValues->removeRow(0);
        // загрзука данных по переменным из файла,
        // если исходное уравнение не изменилось
        QFile formula("formula.txt");
        formula.open(QIODevice::ReadWrite);
        QTextStream stream(&formula);
        stream.readLine();
        if((formula.isOpen())&&(stream.readLine()==ui->editNominator->text())&&
                (stream.readLine()==ui->editDenominator->text())&&
                (!stream.atEnd()))
        {
            ui->spinX->setValue(stream.readLine().toInt());
            ui->doubleSpinH->setValue(stream.readLine().toDouble());
            ui->spinN->setValue(stream.readLine().toInt());
            QString vars = stream.readLine();
            while(!vars.isEmpty())
            {
                if(!vals->add(vars[0]))
                    QMessageBox::critical(NULL,"Слишком много неизвестных",
                                          "Возможно использование только 2-х "
                                          "неизвестных.");
                ui->tableValues->insertRow(ui->tableValues->rowCount());
                ui->tableValues->setItem(ui->tableValues->rowCount()-1,0,
                                         new QTableWidgetItem(QChar(vars[0])));
                vars.remove(0,2);
                ui->tableValues->setItem(ui->tableValues->rowCount()-1,1,
                                         new QTableWidgetItem(vars));
                vars = stream.readLine();
            }
        }
        else
        {
            if(ui->radioDouble->isChecked())
                stream<<"double"<<endl;
            else
                stream<<"int"<<endl;
            stream<<ui->editNominator->text()<<endl<<ui->editDenominator->text()<<endl;
            for (const auto& c:str)
                if((c.isLetter())&&(c!='s'))
                {
                    if(!vals->contains(c))
                    {
                        if(!vals->add(c))
                            QMessageBox::critical(NULL,"Слишком много неизвестных",
                                                  "Возможно использование только 2-х "
                                                  "неизвестных.");;
                        ui->tableValues->insertRow(ui->tableValues->rowCount());
                        ui->tableValues->setItem(ui->tableValues->rowCount()-1,0,
                                                 new QTableWidgetItem(c));
                        ui->tableValues->setItem(ui->tableValues->rowCount()-1,1,
                                                 new QTableWidgetItem(0));
                    }
                }
        }
        // загрузка делегатов в таблицу
        for (int i=0; i<ui->tableValues->rowCount();++i)
        {
            Type *spin = new Type();
            spin->setFrame(false);
            spin->setMinimum(-100);
            spin->setMaximum(100);
            qDebug()<<ui->tableValues->item(i,1)->text();
            spin->setValue(static_cast<T>(ui->tableValues->item(i,1)->text().toDouble()));
            if(std::is_same<T,double>::value)
                spin->setSingleStep(0.5);
            ui->tableValues->setCellWidget(i,1,spin);
            ui->tableValues->setCellWidget(i,2,new QCheckBox());
        }
        ui->groupCalcData->setEnabled(true);
        ui->groupResult->setEnabled(true);
        formula.close();
    }

    // предварительные рассчеты и отрисовка графика
    void calculateYTheoretical()
    {
        //занесение переменных в таблицу значений и сохранение данных в файл
        QFile formula("formula.txt");
        formula.open(QIODevice::WriteOnly);
        QTextStream stream(&formula);
        if(ui->radioDouble->isChecked())
            stream<<"double"<<endl;
        else
            stream<<"int"<<endl;
        stream<<ui->editNominator->text()<<endl<<ui->editDenominator->text()<<endl;
        stream<<ui->spinX->text()<<endl;
        stream<<QString::number(ui->doubleSpinH->value())<<endl;
        stream<<ui->spinN->text()<<endl;
        for(int i=0; i<ui->tableValues->rowCount(); ++i)
        {
            vals->remove(ui->tableValues->item(i,0)->text().at(0));

            if(!vals->add(ui->tableValues->item(i,0)->text().at(0),
                          static_cast<Type*>
                          (ui->tableValues->cellWidget(i,1))->value(),
                          static_cast<QCheckBox*>
                          (ui->tableValues->cellWidget(i,2))->isChecked()))
                QMessageBox::critical(NULL,"Слишком много неизвестных",
                                      "Возможно использование только 2-х "
                                      "неизвестных.");
            stream<<ui->tableValues->item(i,0)->text().at(0)<<" "<<
                    vals->getValue(ui->tableValues->item(i,0)->text().at(0))<<endl;
        }
        vals->show();
        formula.close();
        // разбор числителя и знаменателя с выставлением параметров коэфициентам
        int firstpos = 0;
        std::vector<double>* y = parseString(ui->editNominator->text(),
                                             firstpos, vals);
        for(const auto& dbl: (*y))
            qDebug()<<dbl;
        firstpos = 0;
        std::vector<double>* x = parseString(ui->editDenominator->text(),
                                             firstpos, vals);
        for(const auto& dbl: (*x))
            qDebug()<<dbl;
        if ((y == nullptr)||(x == nullptr))
            return;
        if(y->size()>x->size())
        {
            QMessageBox::critical(NULL,"Некорректные данные",
                                  QString("Невозможно вычисление методом "
                                          "Эйлера: порядок числителя больше "
                                          "порядка знаменателя. За "
                                          "разъяснением - к учителю, за фиксами "
                                          "- к разработчику."));
            return;
        }
        yT = calcEuler(y,x, ui->doubleSpinH->value(),
                       double(ui->spinX->value()), ui->spinN->value());
        ui->editLimitFinish->setText(
                    QString::number(-1*double(-ui->spinX->value())*
                                    ((*y)[0]/(*x)[0])));
        if((*y)[x->size()-1])
            ui->editLimitStart->setText(
                        QString::number(-1*double(-ui->spinX->value())*
                                        ((*y)[x->size()-1]/(*x)[x->size()-1])));
        else
            ui->editLimitStart->setText("0");
        ui->editStartPoint->setText(QString::number(yT->front()));
        ui->editFinishPoint->setText(QString::number(yT->back()));
        ui->editMaxPoint->setText
                (QString::number(*std::max_element(yT->begin(),
                                                   yT->end())));
        ui->editMinPoint->setText
                (QString::number(*std::min_element(yT->begin(),
                                                   yT->end())));
        // настройка графика
        ui->wdgGraph->clearGraphs();
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(0)->setName("yT");
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(1)->setName("yШ1");
        ui->wdgGraph->graph(1)->setPen(QPen(Qt::red));
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(2)->setName("yШ2");
        ui->wdgGraph->graph(2)->setPen(QPen(Qt::green));
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(3)->setName("yШ3");
        ui->wdgGraph->graph(3)->setPen(QPen(Qt::darkYellow));
        ui->wdgGraph->legend->setVisible(true);
        QPen pen;
        pen.setStyle(Qt::DashLine);
        pen.setColor(Qt::red);
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(4)->setName("yМ1");
        ui->wdgGraph->graph(4)->setPen(pen);
        pen.setColor(Qt::green);
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(5)->setName("yМ2");
        ui->wdgGraph->graph(5)->setPen(pen);
        pen.setColor(Qt::darkYellow);
        ui->wdgGraph->addGraph();
        ui->wdgGraph->graph(6)->setName("yМ3");
        ui->wdgGraph->graph(6)->setPen(pen);
        ui->wdgGraph->axisRect()->insetLayout()->setInsetAlignment
                (0, Qt::AlignRight|Qt::AlignBottom);
        ui->wdgGraph->xAxis->setLabel("n");
        ui->wdgGraph->yAxis->setLabel("y");
        ui->wdgGraph->xAxis->setRange(0,ui->spinN->value());
        if ((tableY) && (!tableY->isHidden()))
            updateTableY();
        drawGraphics(yT);
    }

    // перерисовывает график
    void drawGraphics(const std::vector<double>* vct, int position = 0,
                      bool isReplotNeeded = true)
    {
        if (*vct!=std::vector<double>())
        {
            // вектор с номерами итераций для оси x
            QVector<double> tempV = QVector<double>();
            for(int i = 0; i < ui->spinN->value()+1; ++i)
            {
                tempV.push_back(double(i));
            }
            ui->wdgGraph->graph(position)->setData(
                        tempV,QVector<double>::fromStdVector(*vct));
            double minrange = *std::min_element(vct->begin(), vct->end())*1.3;
            double maxrange = *std::max_element(vct->begin(),vct->end())*1.3;
            if(ui->wdgGraph->yAxis->range().lower < minrange)
                minrange = ui->wdgGraph->yAxis->range().lower;
            if(ui->wdgGraph->yAxis->range().upper > maxrange)
                maxrange = ui->wdgGraph->yAxis->range().upper;
            ui->wdgGraph->yAxis->setRange(minrange,maxrange);
        }
        if (isReplotNeeded)
            ui->wdgGraph->replot();
    }

    // перемножает элементы вектора, как коэффициенты скобок
    std::vector<double>* multiply(std::vector<double>* v1,
                                  std::vector<double>* v2)
    {
        /*
         * размер вектора равен (size v1-1)+ (size v2-1)+1
         * результат записывается в ячейку нового вектора по сумме ячеек предыдущих
         */
        //qDebug()<<"multiplying";
        std::vector<double>* res= new std::vector<double>(v1->size()+v2->size()-1, 0.0);
        for (int i = 0; i<v1->size();++i)
            for (int j = 0; j<v2->size();++j)
                (*res)[i+j] += (*v1)[i]*(*v2)[j];
        return res;
    }

    // перегружена для векторов и единичных значений
    // функция запрашивает значение степени и умножает поствпивший параметр на
    // себя необходимое количество раз
    // UB: если findPower возвращает 0
    std::vector<double>* checkPower(const QString& str, int& place,
                                    std::vector<double>* v1)
    {
        //qDebug()<<"checkingPower"<<place<<str[place];
        int power = findPower(str,place);
        //qDebug()<<"power is"<<power<<place<<str[place];
        while(power>1)
        {
            v1 = multiply(v1,v1);
            --power;
        }
        return v1;
    }

    double checkPower(const QString& str, int& place,double v1)
    {
        //qDebug()<<"checkingPower"<<place<<str[place];
        int power = findPower(str,place);
        //qDebug()<<"power is"<<power<<place<<str[place];
        while(power>1)
        {
            v1 = v1*v1;
            --power;
        }
        return v1;
    }

    // функция получает значение степени(только целочисленные константы)
    // и переносит курсор в позицию после степени
    int findPower(const QString& str, int& place)
    {
        int power = 1;
        if (place<str.size())
        {
            if (str[place]== '^')
            {
                ++place;
                if(str[place].isNumber())
                {
                    power = getNumber(str, place);
                }
                else
                {
                    QMessageBox::critical(NULL,"Ошибка в формуле",
                                          "Вслед за знаком степени может идти "
                                          "только число");
                    power = 0;
                }
            }
        }
        return power;
    }

    // записывает значения текущего вектора в постоянный и обнуляет текущий
    void commitVector(std::vector<double>* permanentVct,
                      std::vector<double>* currentVct)
    {
        //qDebug()<<"commiting";
        while (currentVct->size()>permanentVct->size())
            permanentVct->push_back(0.0);
        while (currentVct->size()<permanentVct->size())
            currentVct->push_back(0.0);
        std::transform(permanentVct->begin(),permanentVct->end(),
                       currentVct->begin(), permanentVct->begin(),
                       std::plus<double>());
        for(auto& dbl:(*currentVct))
            dbl = 0.0;
    }

    // Алгоритм побуквенного чтения десятичного числа с переносом курсора
    int getNumber(const QString& str, int& place)
    {
        int thisNumber = str[place].digitValue();
        ++place;
        //qDebug()<<"getnmber"<<place<<str.size();
        while ((place<str.size())&&(str[place].isNumber()))
        {
            thisNumber = thisNumber * 10 + str[place].digitValue();
            ++place;
        }
        return thisNumber;
    }

    // NB: Индекс subStringIndex является локальной копией firstPos =>
    // Необходимо перезаписать обратно перед выходом

    // функция парсит строку и возвращает значения в виде вектора коэффициентов
    // s, где индекс элемента означает степень s.
    // для перехода в новые скобки вызывает себя же для подстроки
    // не жует скобки под дробью и в степени
    std::vector<double>* parseString(const QString& str, int& firstPos,
                                     ValTable<double>* _vals = new ValTable<T>,
                                     std::vector<double>* vct
                                     = new std::vector<double>(1, 0.0))
    {
        int subStringIndex = firstPos;
        int currentPower = 0;
        // Единица так как коэффициенты - произведения
        std::vector<double>* currentValue = new std::vector<double>(1, 1.0);
        bool sign = true; // проверка двойного знака
        while(str.size()>subStringIndex)
        {
            if(str[subStringIndex].isLetter())
            {
                if(str[subStringIndex]=='s')
                {
                    /* при получении S увеличивает размер векторов на одну
                     * степень. Важно: отрицательные степени и s в знаменателе
                     * не поддерживаются, а значит нет необходимости уменьшать
                     * порядок уравнения
                     */
                    //qDebug()<<"isS"<<subStringIndex<<str[subStringIndex];
                    ++subStringIndex;
                    int power = findPower(str, subStringIndex) + currentPower + 1;
                    int prevPower = currentPower;
                    while (power>1)
                    {
                        ++currentPower;
                        --power;
                    }
                    while(!((currentPower)<vct->size()))
                    {
                        vct->push_back(0.0);
                        currentValue->push_back(0.0);
                    }
                    /* в векторе текущих значений всегда хранится только одно
                     * произведение коэффициентов. Так как для записи в
                     * глобальный вектор используется std::transform
                     * приходится очищать остальные значения
                     */
                    (*currentValue)[currentPower] = (*currentValue)[prevPower];
                    (*currentValue)[prevPower] = 0;
                }
                else
                {
                    //qDebug()<<"isLetter"<<subStringIndex<<str[subStringIndex];
                    /* при получении еременной ее значение берется из таблицы
                     * и записывается в виде коэффициента
                     */
                    currentValue = multiply(currentValue,
                                            new std::vector<double>
                                            (1,checkPower(str, ++subStringIndex,
                                                          _vals->getValue
                                                          (str[subStringIndex]))));
                }
                sign = false;
            }
            else if (str[subStringIndex].isNumber())
            {
                //qDebug()<<"isNumber"<<subStringIndex<<str[subStringIndex];
                sign = false;
                currentValue = multiply(currentValue,
                                        new std::vector<double>
                                        (1,checkPower(str, subStringIndex,
                                                      getNumber(str, subStringIndex))));
            }
            else if ((str[subStringIndex] == '-')||
                     (str[subStringIndex] == '+'))
            {
                //qDebug()<<"is+-"<<subStringIndex<<str[subStringIndex];
                /* При появлении знака происходит оправка вектора в массив
                 * Также устанавливается начальное значение для следующего члена
                 */
                if(!sign)
                {
                    sign = true;
                    commitVector(vct, currentValue);
                    currentPower = 0;
                    (*currentValue)[currentPower] = 1.0;
                }
                else if(subStringIndex - firstPos != 0)
                {
                    QMessageBox::critical(NULL,"Ошибка в формуле",
                                          "Два знака подряд на позиции"+
                                          QString::number(subStringIndex));
                    return nullptr;
                }
                if(str[subStringIndex] == '-')
                    (*currentValue)[currentPower] = -1;
                ++subStringIndex;

            }
            else if(str[subStringIndex] == '/')
            {
                //qDebug()<<"is/"<<subStringIndex<<str[subStringIndex];
                if (!sign)
                {
                    /* Деление на целочисленную константу и только на нее.
                     * Число прочитывается сразу же
                     */
                    ++subStringIndex;
                    if (str[subStringIndex].isNumber())
                    {
                        int num = getNumber(str, subStringIndex);
                        sign = false;
                        if(num)
                        {
                            (*currentValue)[currentPower] /= num;
                        }
                        else
                        {
                            QMessageBox::critical(NULL,"Ошибка в формуле",
                                                  "Деление на 0!");
                            return nullptr;
                        }
                    }
                    else
                    {
                        QMessageBox::critical(NULL,"Ошибка в формуле",
                                              "Деление возможно только на "
                                              "целочисленную константу" +
                                              QString::number(subStringIndex));
                    }
                }
                else
                {
                    QMessageBox::critical(NULL,"Ошибка в формуле",
                                          "Два знака подряд на позиции "+
                                          QString::number(subStringIndex));
                    return nullptr;
                }
            }
            else if(str[subStringIndex] == '*')
            {
                //qDebug()<<"is*"<<subStringIndex<<str[subStringIndex];
                /* В соответствии с созданным синтаксисом знак умножить можно
                 * упускать
                 */
                if (!sign)
                {
                    sign = true;
                    ++subStringIndex;
                }
                else
                {
                    QMessageBox::critical(NULL,"Ошибка в формуле",
                                          "Два знака подряд на позиции "+
                                          QString::number(subStringIndex));
                    return nullptr;
                }
            }
            else if(str[subStringIndex] == '^')
            {
                /* Все подходящие случаи появления степени контролируются в
                 * элементах - основаниях. Если находится лишний знак,
                 * выдается сообщение об ошибке
                 */
                //qDebug()<<"is^"<<subStringIndex<<str[subStringIndex];
                QMessageBox::critical(NULL,"Ошибка в формуле",
                                      QString("Некорректное возведение в степень")+
                                      QString::number(subStringIndex));
                return nullptr;
            }
            else if(str[subStringIndex] == '(')
            {
                sign = false;
                // вызов отдельного парсера для подстроки
                //qDebug()<<"is("<<subStringIndex<<str[subStringIndex];
                ++subStringIndex;
                currentValue = multiply(currentValue,
                                        parseString(str,subStringIndex,_vals));
            }
            else if(str[subStringIndex] == ')')
            {
                /* Закрытие скобки вызывает выход из цикла и дальнейшую
                 * отправку данных в родительскую функцию
                 */
                //qDebug()<<"is)"<<subStringIndex<<str[subStringIndex];
                ++subStringIndex;
                commitVector(vct,currentValue);
                if(str.size()>subStringIndex)
                    vct = checkPower(str, subStringIndex, vct);
                break;
            }
            else
            {
                // default
                QMessageBox::critical(NULL,"Ошибка в формуле",
                                      QString("Я забыл о существовании символа ")+
                                      str[subStringIndex]);
                return nullptr;
            }
        }
        if(sign)
        {
            // проверка на провисший знак
            QMessageBox::critical(NULL,"Ошибка в формуле",
                                  QString("Формула заканчивается знаком"));
            return nullptr;
        }
        else
        {
            firstPos = subStringIndex;
            commitVector(vct,currentValue);
            return vct;

        }
    }

    // решает СДУ методом эйлера
    static std::vector<double>* calcEuler(std::vector<double>* y,
                                          std::vector<double>* x,
                                          double h = 0.5,
                                          double xt = 1,
                                          int n = 200)
    {
        xt = -xt;
        std::vector<double>* res= new std::vector<double>();
        while(y->size()!=x->size())
            y->push_back(0.0);
        int max = x->size()-1; // номер максимального элемента
        std::vector<double> z(max+1,0.0);
        // перемножает парами числа и прибалвяет их к сумме
        // x(0;max-1), z(0;max-1), x(t)
        do
        {
            // вычисление n+1-го элемента
            z[max] = z[max-1] + h*(std::inner_product(x->begin(),x->end()-1,z.begin(),
                                                      xt)/(-(*x)[max]));
            // вычисление значения yT
            res->push_back(std::inner_product(z.begin(),z.end(),y->begin(),0.0));
            // вычисление n-ых элементов
            for (int i = 0; i < max-1 ; ++i)
                z[i] = z[i] + h *z[i+1];
            z[max-1] = z[max]; // Zmax,n = Zmax n+1;
            --n;
        } while(n > -1);
        return res;
    }

    // применяет шум к построенному графику
    void applyNoise()
    {
        if (ui->checkNoise1->isChecked())
        {
            // получение случайных изменений
            yN1 = generateRandomVector(
                        *std::max_element(yT->begin(),yT->end())*0.05,
                        ui->cmbDistribution->currentIndex(),
                        yT->size(), false).toStdVector();
            // создание шума
            std::transform(yN1.begin(),yN1.end(),yT->begin(),yN1.begin(),
                           std::plus<double>());
            // отрисовка вместо определенного графа
            drawGraphics(&yN1, 1, false);
        }
        else
            ui->wdgGraph->graph(1)->clearData();
        if (ui->checkNoise2->isChecked())
        {
            yN2 = generateRandomVector(
                        *std::max_element(yT->begin(),yT->end())*0.1,
                        ui->cmbDistribution->currentIndex(),
                        yT->size(), false).toStdVector();
            std::transform(yN2.begin(),yN2.end(),yT->begin(),yN2.begin(),
                           std::plus<double>());
            drawGraphics(&yN2, 2, false);
        }
        else
            ui->wdgGraph->graph(2)->clearData();
        if (ui->checkNoise3->isChecked())
        {
            yN3 = generateRandomVector(
                        *std::max_element(yT->begin(),yT->end())*0.2,
                        ui->cmbDistribution->currentIndex(),
                        yT->size(), false).toStdVector();
            std::transform(yN3.begin(),yN3.end(),yT->begin(),yN3.begin(),
                           std::plus<double>());
            drawGraphics(&yN3, 3, false);
        }
        else
            ui->wdgGraph->graph(3)->clearData();
        if((tableY)&&(!tableY->isHidden()))
            updateTableY();
        replotGraphics();
        ui->btnOptimization->setEnabled(true);
    }

    // вызывает окно с гистограммой с учетом текущего граифка и выбранного коэф.
    void openHistogramWindow()
    {
        if((!hst)|| hst->isHidden())
        {
            if(yT)
            {
                int coef = 2;
                if(ui->checkNoise1->isChecked())
                    coef = 0;
                else if (ui->checkNoise2->isChecked())
                    coef = 1;
                hst = new HistogramWindow
                        (*std::max_element(yT->begin(),yT->end()),
                         ui->cmbDistribution->currentIndex(), coef);
            }
            else
                hst = new HistogramWindow;
            hst->show();
        }
        else
        {
            hst->showNormal();
            hst->activateWindow();
        }
    }

    // cоздание таблицы значений
    void showTableY()
    {
        if(!tableY)
        {
            tableY = new QTableWidget(yT->size(),4);
            tableY->setWindowTitle("Таблица значений Y");
            tableY->setEditTriggers(QAbstractItemView::NoEditTriggers);
            tableY->setHorizontalHeaderItem(0,new QTableWidgetItem("yT"));
            tableY->setHorizontalHeaderItem(1,new QTableWidgetItem("yШ1"));
            tableY->setHorizontalHeaderItem(2,new QTableWidgetItem("yШ2"));
            tableY->setHorizontalHeaderItem(3,new QTableWidgetItem("yШ3"));
            tableY->horizontalHeader()->setSectionResizeMode
                    (QHeaderView::ResizeToContents);
            tableY->horizontalHeader()->setSectionResizeMode
                    (0, QHeaderView::Stretch);
            updateTableY();
            tableY->resize(320, 500);
            tableY->show();
        }
        else
        {
            if(tableY->isHidden())
                updateTableY();
            tableY->showNormal();
            tableY->activateWindow();
        }
    }

    // выгрзука значений в таблицу
    void updateTableY()
    {
        for (int i = 0; i<yT->size(); ++i)
        {
            tableY->setItem(i,0, new QTableWidgetItem
                            (QString::number((*yT)[i])));
            if(!ui->wdgGraph->graph(1)->data()->isEmpty())
                tableY->setItem(i,1, new QTableWidgetItem
                                (QString::number(
                                     ui->wdgGraph->graph(1)->data()
                                     ->value(i).value)));
            else
                tableY->setItem(i,1, new QTableWidgetItem());
            if(!ui->wdgGraph->graph(2)->data()->isEmpty())
                tableY->setItem(i,2, new QTableWidgetItem
                                (QString::number(
                                     ui->wdgGraph->graph(2)->data()
                                     ->value(i).value)));
            else
                tableY->setItem(i,2, new QTableWidgetItem());
            if(!ui->wdgGraph->graph(3)->data()->isEmpty())
                tableY->setItem(i,3, new QTableWidgetItem
                                (QString::number(
                                     ui->wdgGraph->graph(3)->data()
                                     ->value(i).value)));
            else
                tableY->setItem(i,3, new QTableWidgetItem());
        }
    }

    // вызов оптимизации отмеченных функций
    void optimizeUserFunction()
    {
        if(vals->checkUnknownCount())
        {
            if(ui->checkOpt1->isChecked())
            {
                opt = new OptimAlgoWindow;
                ValTable<double>* val = opt->optimizeUserFunction
                        (new EulerData(vals->toDouble(),
                                       ui->doubleSpinH->value(),
                                       ui->spinX->value(), ui->spinN->value(),
                                       ui->editNominator->text(),
                                       ui->editDenominator->text(),
                                       QVector<double>::fromStdVector(yN1)));
                // разбор числителя и знаменателя с выставлением параметров коэфициентам
                int firstpos = 0;
                val->show();
                opt->show();
                std::vector<double>* y = parseString(ui->editNominator->text(),
                                                     firstpos, val);
                firstpos = 0;
                std::vector<double>* x = parseString(ui->editDenominator->text(),
                                                     firstpos, val);
                std::vector<double>* yM = calcEuler(y,x, ui->doubleSpinH->value(),
                                                    double(ui->spinX->value()), ui->spinN->value());
                drawGraphics(yM, 4, false);
            }
            if(ui->checkOpt2->isChecked())
            {
                opt = new OptimAlgoWindow;
                ValTable<double>* val = opt->optimizeUserFunction
                        (new EulerData(vals->toDouble(),
                                       ui->doubleSpinH->value(),
                                       ui->spinX->value(), ui->spinN->value(),
                                       ui->editNominator->text(),
                                       ui->editDenominator->text(),
                                       QVector<double>::fromStdVector(yN2)));
                opt->show();
                // разбор числителя и знаменателя с выставлением параметров коэфициентам
                int firstpos = 0;
                std::vector<double>* y = parseString(ui->editNominator->text(),
                                                     firstpos, val);
                firstpos = 0;
                std::vector<double>* x = parseString(ui->editDenominator->text(),
                                                     firstpos, val);
                std::vector<double>* yM = calcEuler(y,x, ui->doubleSpinH->value(),
                                                    double(ui->spinX->value()), ui->spinN->value());
                drawGraphics(yM, 5, false);
            }
            if(ui->checkOpt3->isChecked())
            {
                opt = new OptimAlgoWindow;
                ValTable<double>* val = opt->optimizeUserFunction
                        (new EulerData(vals->toDouble(),
                                       ui->doubleSpinH->value(),
                                       ui->spinX->value(), ui->spinN->value(),
                                       ui->editNominator->text(),
                                       ui->editDenominator->text(),
                                       QVector<double>::fromStdVector(yN3)));
                opt->show();
                // разбор числителя и знаменателя с выставлением параметров коэфициентам
                int firstpos = 0;
                std::vector<double>* y = parseString(ui->editNominator->text(),
                                                     firstpos, val);
                firstpos = 0;
                std::vector<double>* x = parseString(ui->editDenominator->text(),
                                                     firstpos, val);
                std::vector<double>* yM = calcEuler(y,x, ui->doubleSpinH->value(),
                                                    double(ui->spinX->value()), ui->spinN->value());
                drawGraphics(yM, 6, false);
            }
            replotGraphics();

        }
        else
            QMessageBox::critical(NULL,"Недостаточно неизвестных",
                                  "Для работы алгоритма оптимизации необходимо"
                                  " ровно 2 неизвестные.");
    }

    // перерисовка необходимых графиков
    void replotGraphics()
    {
        ui->wdgGraph->graph(1)->setVisible(ui->checkNoise1->isChecked());
        ui->wdgGraph->graph(2)->setVisible(ui->checkNoise2->isChecked());
        ui->wdgGraph->graph(3)->setVisible(ui->checkNoise3->isChecked());
        ui->wdgGraph->graph(4)->setVisible(ui->checkOpt1->isChecked());
        ui->wdgGraph->graph(5)->setVisible(ui->checkOpt2->isChecked());
        ui->wdgGraph->graph(6)->setVisible(ui->checkOpt3->isChecked());
        ui->wdgGraph->replot();
    }


private:
    Ui::MainWindow *ui;
    ValTable<double>* vals;
    std::vector<double>* yT, yN1, yN2, yN3;
    HistogramWindow *hst;
    OptimAlgoWindow *opt;
    QTableWidget *tableY;
};

#endif // CALCULATIONS_H
