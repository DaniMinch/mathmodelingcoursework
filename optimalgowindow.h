#ifndef OPTIMALGOWINDOW_H
#define OPTIMALGOWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <random>
#include <chrono>
#include <cmath>
#include <algorithm>
#include "ui_optimalgowindow.h"


template<typename T>
class ValTable;

struct EulerData
{
    EulerData(ValTable<double>* _values, double _h, double _xt,
              int _n,const QString& _nom, const QString& _denom,
              const QVector<double>& _yN)
        :value(_values), h(_h), xt(_xt), n(_n), nominator(_nom),
          denominator(_denom), yN(_yN){}
    ValTable<double>* value;
    double h = 0.5, xt = 1.0;
    int n = 100;
    const QString& nominator = "";
    const QString& denominator ="";
    const QVector<double>& yN = QVector<double>(0,1);
} ;

struct Points
{
    //Points();
    //Points(double _x, double _y, double _z): x(_x),y(_y),z(_z){}
    double x;
    double y;
    double z;
};

static bool lessPoint (Points i,Points j) { return (i.z<j.z); }
static bool lessX (Points i,Points j) { return ((i.x<j.x)||((i.x == j.x)&&(i.y<j.y))); }
static bool lessY (Points i,Points j) { return (i.y<j.y); }


namespace Ui {
class OptimAlgoWindow;
}


class OptimAlgoWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit OptimAlgoWindow(QWidget *parent = 0);
    ~OptimAlgoWindow();
    ValTable<double> *optimizeUserFunction(EulerData *data);

private:
    Ui::OptimAlgoWindow *ui;
    void drawIsolines(bool isRosenbrock);
    QVector<QVector<double> *> randomSearchOptimization(
            int functionType = 1,
            // 0 - Rosenbrock, 1 - Ellipsoid
            int A = 1, int B = 1,
            double startX = 1.0, double startY = 1.0,
            double H = 0.02, double E = 0.001);
    QVector<QVector<double> *> randomSearchOptimization(EulerData* data);


private slots:
    void startOptimization();
    void showAbout(){QMessageBox::information(this,"О программе",
                                              "<html><head/><body><p>Данное окно позволяет провести процесс "
                                              "оптимизации (нахождения экстремума(минимума)) двух функций: эллипсоида"
                                              " и функции Розенброка. Оптимизация будет проводиться безградиентным ме"
                                              "тодом случайного поиска с постоянным шагом. График трехмерной функции "
                                              "будет отображаться на плоскости в виде изолиний. Ход поиска будет отоб"
                                              "ражаться на этом же графике линией красного цвета.</p><p>Для инициации"
                                              " процесса оптимизации необходимо:</p><ul style=\"margin-top: 0px; marg"
                                              "in-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: "
                                              "1;\"><li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px;"
                                              " margin-right:0px; -qt-block-indent:0; text-indent:0px;\">ввести значе"
                                              "ния параметров исходной функции;</li><li style=\" margin-top:12px; mar"
                                              "gin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0;"
                                              " text-indent:0px;\">указать количество изолиний и промежуток, через ко"
                                              "торый их надо отображать;</li><li style=\" margin-top:12px; margin-bot"
                                              "tom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-i"
                                              "ndent:0px;\">выбрать начальную точку поиска, его шаг и точность, до ко"
                                              "торой надо проводить поиск;</li><ul style=\"margin-top: 0px; margin-bo"
                                              "ttom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\">"
                                              "<li style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; marg"
                                              "in-right:0px; -qt-block-indent:0; text-indent:0px;\">указать какую час"
                                              "ть плоскости отображать на графике;</li></ul><li style=\" margin-top:1"
                                              "2px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-i"
                                              "ndent:0; text-indent:0px;\">нажать кнопку &quot;Применить&quot;.</li><"
                                              "/ul></body></html>");}
    void chooseFormula(bool);

};

#endif // OPTIMALGOWINDOW_H
