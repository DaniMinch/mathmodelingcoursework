/*Все вычисления происходят в классе Calculations, которому передается указатель
на все обьекты интерфейса. Это сделано для возможности работы с двумя
различными типами даных: int и double, т.к. Макрос Q_OBJECT не работает
c template
*/
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFile formula("formula.txt");
    formula.open(QIODevice::ReadOnly);
    if(formula.isOpen())
    {
        QTextStream in(&formula);
        QString line = in.readLine();
        if(!line.isEmpty())
        {
            if (line.length()>3)
                ui->radioDouble->setChecked(true);
            else
                ui->radioInt->setChecked(true);
        }
        line = in.readLine();
        if(!line.isEmpty())
            ui->editNominator->setText(line);
        line = in.readLine();
        if(!line.isEmpty())
            ui->editDenominator->setText(line);
    }
    formula.close();

    //дополнительная установка поведения компонентов GUI - основная
    //сгенерирована в интерфейсном файле
    ui->tableValues->horizontalHeader()->setSectionResizeMode
            (QHeaderView::ResizeToContents);
    //connect соединяет сигналы от элементов GUI со слотами вызывающими функции
    connect(ui->btnChangeInitial,SIGNAL(clicked(bool)),
            this,SLOT(setVariables()));
    connect(ui->checkAutoRefresh,SIGNAL(toggled(bool)),
            this,SLOT(setAutoRefresh(bool)));
    connect(ui->btnRecalc,SIGNAL(clicked(bool)),
            this,SLOT(drawGraphics()));
    connect(ui->btnReset,SIGNAL(clicked(bool)),
            this,SLOT(resetInitial()));
    connect(ui->btnHistogram,SIGNAL(clicked(bool)),
            this,SLOT(openHistogram()));
    connect(ui->btnApplyNoise,SIGNAL(clicked(bool)),
            this,SLOT(applyNoise()));
    connect(ui->btnShowTableY,SIGNAL(clicked(bool)),
            this,SLOT(showTableY()));
    connect(ui->btnOptimization,SIGNAL(clicked(bool)),
            this,SLOT(optimizeUserFunction()));
    connect(ui->actionHisto,SIGNAL(triggered(bool)),
            this,SLOT(openHistogram()));
    connect(ui->actionAbout,SIGNAL(triggered(bool)),
            this,SLOT(openAbout()));
    connect(ui->actionExit,SIGNAL(triggered(bool)),
            this,SLOT(close()));
    connect(ui->actionOptimAlgo,SIGNAL(triggered(bool)),
                this,SLOT(openOptAlgo()));
    ui->wdgGraph->setInteractions(QCP::iRangeZoom|QCP::iRangeDrag);
    //вызов слота, чтобы не нажимать кнопку каждый раз
    setVariables();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*SLOT*/ void MainWindow::resetInitial()
{
    ui->checkAutoRefresh->setChecked(false);
    ui->tableValues->clearContents();
    ui->spinN->setValue(200);
    ui->spinX->setValue(1);
    ui->doubleSpinH->setValue(0.5);
    ui->groupCalcData->setEnabled(false);
    ui->groupNumType->setEnabled(true);
    ui->groupResult->setEnabled(false);
    ui->frameFormula->setEnabled(true);
    ui->btnChangeInitial->setEnabled(true);
    ui->editLimitStart->clear();
    ui->editLimitFinish->clear();
    ui->wdgGraph->clearGraphs();
    ui->wdgGraph->replot();
    ui->frameGraphData->setEnabled(false);
    ui->groupNoise->setEnabled(false);
    ui->btnOptimization->setEnabled(false);
    delete calcDouble;
    delete calcInt;
}

//установка переменных в таблицу => вызов одноименной функции
/*SLOT*/void MainWindow::setVariables()
{
    calcInt = new Calculations<int,QSpinBox>(ui);
    calcDouble = new Calculations<double,QDoubleSpinBox>(ui);
    if (ui->radioDouble->isChecked())
    {
        //calcDouble = new Calculations<double,QDoubleSpinBox>(ui);
        calcDouble->setVariables(ui->editNominator->text() +
                                 ui->editDenominator->text());
    }
    else
    {
        //calcInt = new Calculations<int,QSpinBox>(ui);
        calcInt->setVariables(ui->editNominator->text() +
                              ui->editDenominator->text());
    }

}

// подключение сигналов для автоматического обновления данных
/*SLOT*/void MainWindow::setAutoRefresh(bool isAutoEnabled)
{
    if(isAutoEnabled)
    {
        for(int i=0; i<ui->tableValues->rowCount();++i)
            connect(ui->tableValues->cellWidget(i,1),
                    SIGNAL(valueChanged(QString)),
                    this,SLOT(drawGraphics()));
        connect(ui->spinX,SIGNAL(valueChanged(int)),
                this,SLOT(drawGraphics()));
        connect(ui->spinN,SIGNAL(valueChanged(int)),
                this,SLOT(drawGraphics()));
        connect(ui->doubleSpinH,SIGNAL(valueChanged(double)),
                this,SLOT(drawGraphics()));
    }
    else
    {
        for(int i=0; i<ui->tableValues->rowCount();++i)
            disconnect(ui->tableValues->cellWidget(i,1),
                       SIGNAL(valueChanged(int)),
                       this,SLOT(drawGraphics()));
        disconnect(ui->spinX,SIGNAL(valueChanged(int)),
                   this,SLOT(drawGraphics()));
        disconnect(ui->spinN,SIGNAL(valueChanged(int)),
                   this,SLOT(drawGraphics()));
        disconnect(ui->doubleSpinH,SIGNAL(valueChanged(double)),
                   this,SLOT(drawGraphics()));
    }
}

// вычисление yT
/*SLOT*/void MainWindow::drawGraphics()
{
    if (ui->radioDouble->isChecked())
        calcDouble->calculateYTheoretical();
    else
        calcInt->calculateYTheoretical();
    ui->groupNoise->setEnabled(true);
    ui->frameGraphData->setEnabled(true);
    ui->btnOptimization->setEnabled(false);
}

// вызывает окно с гистограммой ГСЧ
/*SLOT*/void MainWindow::openHistogram()
{
    if (ui->radioDouble->isChecked())
        calcDouble->openHistogramWindow();
    else
        calcInt->openHistogramWindow();
}

// применяет выбранный шум к графику
/*SLOT*/void MainWindow::applyNoise()
{
    if (ui->radioDouble->isChecked())
        calcDouble->applyNoise();
    else
        calcInt->applyNoise();
}

/*SLOT*/void MainWindow::showTableY()
{
    if (ui->radioDouble->isChecked())
        calcDouble->showTableY();
    else
        calcInt->showTableY();
}

/*SLOT*/void MainWindow::optimizeUserFunction()
{
    if (ui->radioDouble->isChecked())
        calcDouble->optimizeUserFunction();
    else
        calcInt->optimizeUserFunction();
}
